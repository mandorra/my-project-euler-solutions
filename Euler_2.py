#Execution time  0.000309944152832 seconds

import numpy as np
import time

start_time = time.time()

a=np.array([1,2],dtype=int)
result=0
counter= 2
iteration=2
while(counter<3500000):
    a=np.append(a,a[iteration-2]+a[iteration-1])
    counter=a[iteration]
    iteration=iteration+1

for members in a:
    if (members%2==0):
        result=result+members

print "Execution time ", time.time() - start_time, "seconds"

print "The result is "+str(result)
