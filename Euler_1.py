#Execution time  0.00279378890991 seconds

import numpy as np
import time

start_time = time.time()


a=np.array([],dtype=int)
b=np.array([],dtype=int)
result=0

counter=3
while (counter < 1000):
    a=np.append(a,counter)
    counter=counter+3

counter=5
while (counter < 1000):
    b=np.append(b,counter)
    counter=counter+5

c=np.intersect1d(a,b,assume_unique=True)
d=np.setdiff1d(a,b,assume_unique=True)
e=np.setdiff1d(b,a,assume_unique=True)

c=np.append(c,d)
c=np.append(c,e)

for multiples in c:
    result=result+multiples

print "Execution time ", time.time() - start_time, "seconds"

print "The result is "+str(result)
