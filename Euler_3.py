#Execution time  0.0162260532379 seconds

import time
import math


start_time = time.time()


primes = set([2])
value = 3
number = 600851475143
while value < math.sqrt(number):
    isPrime = True
    for k in primes:
        if value % k == 0:
            isPrime = False
            value += 2
    if isPrime:
        primes.add(value)
        if number % value == 0:
            number /= value

print "Execution time ", time.time() - start_time, "seconds"

print "The result is: "+str(number)
